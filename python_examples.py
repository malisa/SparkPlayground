#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  first.py
#  
#  Copyright 2016  <walter@hyperion>
#  

#from pyspark import SparkContext


def add(x, y):
  return x + y

def reducedemo():
  # range(1,5) => [1,2,3,4]
  print(reduce(add, range(1,5)))
  # 10


def mapdemo():
  print(map(lambda x: x+1, range(1,5)))
  # [2, 3, 4, 5]

  print(map(lambda t: t[0], [(1,2),(3,4),(5,6)]))
  # [1, 3, 5]

  print(reduce(lambda x,y: x+y, map(lambda t: t[0], [(1,2),(3,4),(5,6)])))
  # 9


def demo_a():
  print("A")
  # sum of odd first elements
  a = [(1,2),(3,4),(5,6),(8,9)]
  print(reduce(lambda x,y: x+y, map(lambda x: x if x%2==1 else 0, map(lambda x: x[0], a))))


def demo_b():
  print("B")
  print("(1,2) + (3,4) = " + str((1,2) + (3,4)))
  #b = reduce(lambda x,y: x + y, a)
  b = reduce(lambda x,y: list(x).append(y) if x != None else [x, y], a)
  print(b)


def demo_c():
  print("C")
  c = map(lambda t: range(t[0], t[1]), [(1,5),(7,10)])
  print(c)


def demo_d():
  print("D")
  seconds = 16806.9
  m, s = divmod(seconds, 60)
  h, m = divmod(m, 60)
  print("%.2f seconds = %d:%02d:%02d" % (seconds, h, m, s))


def demo_e():
  print("D")
  seconds = 16806.9
  m, s = divmod(seconds, 60)
  h, m = divmod(m, 60)
  print("%.2f seconds = %d:%02d:%02d" % (seconds, h, m, s))


def demo_f():
  bignumber = 321654987
  bignumberString = "{:,}".format(bignumber)
  print(bignumberString)


def stringLength():
  text = "asdf"
  print("Text: \"" + text + "\"")
  print("Text length: " + str(len(text)))


def stringInList():
  text = ["asdf", "sdfg", "dfgh", "a", "the"]
  word = "dfgh"
  if word in text:
    print("Word \"" + word + "\" found in text list: " + repr(text))
  else:
    print("Word \"" + word + "\" not found in text list: " + repr(text))


def demojoin():
  x = ["asdf", "sdfg", "dfgh"]
  y = " ".join(x)
  print(y)
  print("type(x)" + repr(type(x)))
  print("type(y)" + repr(type(y)))


def printSeparator():
  print("-------------------------------------------------------------------------------")
  

def printTime(timeToPrint, label):
  printSeparator()
  print(label + " " + str(time.strftime("%X", time.localtime(timeToPrint))))
  printSeparator()


def main():
  printSeparator()
  
  demojoin()
  
  printSeparator()
  
  return 0

if __name__ == '__main__':
  main()
