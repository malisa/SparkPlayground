#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  wikipedia_analyzer.py
#  
#  Taken from https://rideondata.wordpress.com/2015/06/29/analyzing-wikipedia-text-with-pyspark/

import time
import re
from pyspark import SparkContext, SparkConf


_appName = "Wikipedia Analyzer"
_threadCount = 7
_master = "local[" + str(_threadCount) + "]"
_datapath = "app/data/"
_logFilename = "app/wiki_output.log"
_maxProcessedLines = 0



def printSeparator():
  writeLog("-------------------------------------------------------------------------------")


def printTime(timeToPrint, label):
  printSeparator()
  writeLog(label + " " + str(time.strftime("%X", time.localtime(timeToPrint))))
  printSeparator()


def writeLog(message):
  with open(_logFilename, "a") as logfile:
    logfile.write(str(message) + "\n")
  print(message)


def printDuration(startTime):
  endTime = time.time()
  writeLog("\n")
  printSeparator()
  printTime(startTime, "Start time:")
  printTime(endTime, "End time:")
  duration = endTime - startTime
  m, s = divmod(duration, 60)
  h, m = divmod(m, 60)
  writeLog("Duration: " + str(round(duration, 2)) + " seconds  [%d:%02d:%02d]" % (h, m, s))
  printSeparator()


def removePunctuation(text):
  text = text.lower().strip()
  text = re.sub("<[^>]+>", "", text) # remove xml tags
  text = re.sub("&[^;]+;", "", text) # remove special characters (like &gt;)
  text = re.sub("[^0-9a-zA-Z ]", "", text) # leave only alpha-numeric characters
  return text


def cleanText(text):
  text = text.lower().strip()
  text = re.sub("<[^>]+>", "", text) # remove xml tags
  text = re.sub("&[^;]+;", "", text) # remove special characters (like &gt;)
  #text = re.sub("[\=\|\(\)\[\]\{\}]", "", text) # remove known non-alphanumeric characters
  return text


def wordCount(wordListRDD):
  return wordListRDD.map(lambda x: (x, 1)).reduceByKey(lambda a,b: a + b)


def totalWordCount():
  wikiFilename = _datapath + "enwiki-latest-pages-articles.xml"
  conf = SparkConf().setAppName(_appName).setMaster(_master)
  sc = SparkContext(conf=conf)
  
  wikipediaRDD = sc.textFile(wikiFilename, 8).map(removePunctuation)
  
  wikipediaWordsRDD = wikipediaRDD.flatMap(lambda x: x.split(" "))
  
  wikipediaWordCount = wikipediaWordsRDD.count();
  writeLog("\nTotal word count:")
  writeLog("{:,}".format(wikipediaWordCount))
  
  sc.stop()


def someRows():
  wikiFilename = _datapath + "enwiki-latest-pages-articles.xml"
  conf = SparkConf().setAppName(_appName).setMaster(_master)
  sc = SparkContext(conf=conf)
  
  wikipediaRDD = sc.textFile(wikiFilename, 8)
  
  # make word count top list
  topWordsCount = 50
  topWordsAndCounts = wikipediaRDD.take(topWordsCount) # take is an Action and executes immediately
  
  writeLog("\nFirst " + str(topWordsCount) + " rows:")
  for wordRec in topWordsAndCounts:
    writeLog(str(wordRec))
  
  sc.stop()


def analyze():
  wikiFilename = _datapath + "enwiki-latest-pages-articles.xml"
  conf = SparkConf().setAppName(_appName).setMaster(_master)
  sc = SparkContext(conf)
  
  wikipediaRDD = sc.textFile(wikiFilename, 8).map(removePunctuation)
  
  if _maxProcessedLines > 0:
    writeLog("Sampling " + "{:,}".format(_maxProcessedLines) + " lines")
    wikipediaRDD = wikipediaRDD.sample(False, _maxProcessedLines, 0)
  
  writeLog("\nTotal line count:")
  wikipediaLineCount = wikipediaRDD.count();
  writeLog("{:,}".format(wikipediaLineCount))
  
  # load file and split it into words
  wikipediaWordsRDD = wikipediaRDD.flatMap(lambda x: x.split(" "))
  
  # filter out empty strings
  wikiWordsRDD = wikipediaWordsRDD.filter(lambda x: x != "")
  
  # tell spark to cache it
  writeLog("Caching words RDD...")
  wikiWordsRDD.cache()
  
  # count all words
  writeLog("\nTotal word count:")
  wikipediaWordCount = wikiWordsRDD.count();
  writeLog("{:,}".format(wikipediaWordCount))
  
  #wikiWordsRDD = wikipediaWordsRDD.filter(lambda x: x != "" and x != "the" and x != "of" and x != "in" and x != "and" and x != "to" and x != "a" 
  #  and x != "page" and x != "is" and x != "for" and x != "revision" and x != "contributor" and x != "on" and x != "was" and x != "as" and x != "by" and x != "or")
  
  # make word count top list
  topWordsCount = 50
  topWordsAndCounts = wordCount(wikiWordsRDD).takeOrdered(topWordsCount, key = lambda (k,v): -v) # takeOrdered is an Action and executes immediately
  
  writeLog("\nTop " + str(topWordsCount) + " words and count:")
  for wordRec in topWordsAndCounts:
    writeLog(str(wordRec))
  
  sc.stop()
  wikiFilename = _datapath + "enwiki-latest-pages-articles.xml"
  conf = SparkConf().setAppName(_appName).setMaster(_master)
  sc = SparkContext(conf=conf)
  
  wikipediaRDD = sc.textFile(wikiFilename, 8)
  
  # make word count top list
  topWordsCount = 50
  topWordsAndCounts = wikipediaRDD.take(topWordsCount) # take is an Action and executes immediately
  
  writeLog("\nFirst " + str(topWordsCount) + " rows:")
  for wordRec in topWordsAndCounts:
    writeLog(str(wordRec))
  
  sc.stop()


# taken from http://www.mccarroll.net/blog/pyspark2/
def analyzeWordPairs(startTime):
  wikiFilename = _datapath + "enwiki-latest-pages-articles.xml"
  sc = SparkContext(_master, _appName)
  
  # create words RDD
  writeLog("\nCreating words RDD...")
  words = sc.textFile(wikiFilename, 8) \
    .map(cleanText) \
    .flatMap(lambda x: x.split(" ")) \
    .filter(lambda x: len(x) > 3)
  
  printDuration(startTime)
  
  writeLog("\nTotal words count:")
  wordsCount = words.count();
  writeLog("{:,}".format(wordsCount))
  
  # isolate sentences
  writeLog("\nIsolating sentences...")
  sentences = sc.textFile(wikiFilename, 8) \
    .map(cleanText) \
    .glom() \
    .map(lambda x: " ".join(x)) \
    .flatMap(lambda x: x.split(".")) \
    .filter(lambda x: x != "") \
    .filter(lambda x: len(x) > 3)
    #.filter(lambda x: len(x) > 3 and x not in ["the", "of", "in", "and", "to", "a", "page", "is", "for", "revision", "contributor", "on", "was", "as", "by", "or"])
  
  printDuration(startTime)
  
  writeLog("\nTotal sentences count:")
  sentencesCount = sentences.count();
  writeLog("{:,}".format(sentencesCount))
  
  printDuration(startTime)
  
  # extract word pairs (also called word bigrams)
  bigrams = sentences \
    .flatMap(lambda x: x.split(" ")) \
    .filter(lambda x: len(x) > 3) \
    .flatMap(lambda x: [((x[i], x[i+1]), 1) for i in range(0, len(x)-1)])
  
  # tell spark to cache it
  writeLog("\nCaching word pairs RDD...")
  bigrams.cache()
  
  printDuration(startTime)
  
  # count all words
  writeLog("\nTotal word pairs count:")
  bigramsCount = bigrams.count();
  writeLog("{:,}".format(bigramsCount))
  
  printDuration(startTime)
  
  # make word count top list
  writeLog("\nCreating word pairs top list...")
  toplistSize = 100
  toplist = bigrams.reduceByKey(lambda x,y: x + y) \
  .map(lambda x: (x[1], x[0])) \
  .sortByKey(False) \
  .take(toplistSize)
  
  printDuration(startTime)
  
  writeLog("\nTop " + str(toplistSize) + " word pairs and count:")
  for rec in toplist:
    writeLog(str(rec[1]) + "\t  " + "{:,}".format(rec[0]))
  writeLog("\nTop " + str(toplistSize) + " word pairs and count:")
  for rec in toplist:
    writeLog("\"" + rec[1][0] + " " + rec[1][1] + "\"\t  " + "{:,}".format(rec[0]))
    
  sc.stop()


def analyzeWords(startTime):
  wikiFilename = _datapath + "enwiki-latest-pages-articles.xml"
  sc = SparkContext(_master, _appName)
  
  # isolate sentences
  writeLog("\nSplitting sentences...")
  #~ sentences = sc.textFile(wikiFilename, 8) \
    #~ .glom() \
    #~ .map(lambda x: " ".join(x)) \
    #~ .map(cleanText) \
    #~ .map(lambda x: re.sub("http[^ ]+", "", x)) \
    #~ .map(lambda x: re.sub("[\ ]+", " ", x)) \
    #~ .map(lambda x: re.sub("\[[^\]]+\]", "", x)) \
    #~ .map(lambda x: re.sub("\{[^\}]+\}", "", x)) \
    #~ .map(lambda x: re.sub("[0-9]+\.+[0-9]+", "", x)) \
    #~ .flatMap(lambda x: x.split(". "))
  sentences = sc.textFile(wikiFilename, 8) \
    .glom() \
    .map(lambda x: re.sub("\<siteinfo\>.+\<\/siteinfo\>", "", x))
  
  #~ writeLog("\nCounting...")
  #~ writeLog("Count: " + "{:,}".format(sentences.count()))
  
  toplistSize = 100
  writeLog("\nTop " + str(toplistSize) + " sentences:")
  toplist = sentences.take(toplistSize)
  for rec in toplist:
    writeLog(repr(rec))
  
  sc.stop()



def topList():
  toplistSize = 100
  wikiFilename = _datapath + "enwiki-latest-pages-articles.xml"
  sc = SparkContext(_master, _appName)
  
  # isolate sentences
  writeLog("\nSplitting sentences...")
  sentences = sc.textFile(wikiFilename, 8) \
    .map(lambda x: x.strip()) \
    .filter(lambda x: x != "<siteinfo>") \
    .filter(lambda x: x != "</siteinfo>") \
    .filter(lambda x: x != "<page>") \
    .filter(lambda x: x != "</page>") \
    .filter(lambda x: x != "<namespaces>") \
    .filter(lambda x: x != "</namespaces>") \
    .filter(lambda x: x != "<revision>") \
    .filter(lambda x: x != "</revision>") \
    .filter(lambda x: x != "<contributor>") \
    .filter(lambda x: x != "</contributor>") \
    .filter(lambda x: x != "<minor />") \
    .filter(lambda x: not x.startswith("<mediawiki")) \
    .filter(lambda x: not x.startswith("<sitename")) \
    .filter(lambda x: not x.startswith("<dbname")) \
    .filter(lambda x: not x.startswith("<base")) \
    .filter(lambda x: not x.startswith("<generator")) \
    .filter(lambda x: not x.startswith("<case")) \
    .filter(lambda x: not x.startswith("<namespace")) \
    .filter(lambda x: not (x.startswith("<ns>") and x.endswith("</ns>"))) \
    .filter(lambda x: not (x.startswith("<id>") and x.endswith("</id>"))) \
    .filter(lambda x: not (x.startswith("<parentid>") and x.endswith("</parentid>"))) \
    .filter(lambda x: not (x.startswith("<redirect") and x.endswith("/>"))) \
    .filter(lambda x: not (x.startswith("<timestamp>") and x.endswith("</timestamp>"))) \
    .filter(lambda x: not (x.startswith("<username>") and x.endswith("</username>"))) \
    .filter(lambda x: not (x.startswith("<comment>") and x.endswith("</comment>"))) \
    .filter(lambda x: not (x.startswith("<model>") and x.endswith("</model>"))) \
    .filter(lambda x: not (x.startswith("<format>") and x.endswith("</format>"))) \
    .filter(lambda x: not (x.startswith("<sha1>") and x.endswith("</sha1>"))) \
    .map(lambda x: re.sub("\{\{[^\}]+\}\}", "", x)) \
    .map(lambda x: re.sub("<text[^>]*>#REDIRECT \[\[[^\]]\]\]", "", x)) \
    .map(lambda x: re.sub("\<title\>(.+)(?:\</title\>)", "\g<1>", x)) \
    .map(lambda x: re.sub("\<text[^\>]+>#REDIRECT \[\[.+((?:\]\])|(?:\<\/text/>))?", "", x)) \
    .map(lambda x: re.sub("\<text[^\>]+>", "", x)) \
    .filter(lambda x: x != "</text>") \
    .filter(lambda x: x != "") \
    .map(lambda x: re.sub("\[\[([^\|][^]]+)\|([^]]+)\]\]", "\g<2>", x)) \
    .map(lambda x: re.sub("\[\[([^\|][^]]+)\]\]", "\g<1>", x)) \
    .map(lambda x: x.replace("&lt;", "<").replace("&gt;", ">").replace("&quot;", "\"").replace("'''", "")) \
    #~ .glom() \
    #~ .map(lambda x: " ".join(x)) \
    #~ .map(lambda x: re.sub("<ref[^>]*>.+(?:</ref>)", "", x)) \
    #~ .map(lambda x: re.sub("<ref[^>]*/>", "", x))
    #.map(lambda x: x.replace("&lt;ref&gt;", "").replace("&lt;/ref&gt;", "").replace("&quot;", "\"").replace("'''", "\""))
  
  writeLog("\nTop " + str(toplistSize) + " sentences:")
  toplist = sentences.take(toplistSize)
  for rec in toplist:
    writeLog(repr(rec))
  
  sc.stop()



def main():
  writeLog("\n")
  printSeparator()
  startTime = time.time()
  printTime(startTime, "Start time:")
  
  try:
    #totalWordCount()
    #printDuration(startTime)
    
    #analyze()
    #analyzeWordPairs(startTime)
    #analyzeWords(startTime)
    topList()
    
    #someRows()
  except Exception as ex:
    writeLog(ex)
  
  printDuration(startTime)
  writeLog("Threads: " + str(_threadCount))
  printSeparator()
  writeLog("\n")
  
  return 0

if __name__ == '__main__':
  main()

